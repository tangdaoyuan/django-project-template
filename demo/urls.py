"""
Demo Router
"""
from django.urls import path
from demo import views

urlpatterns = [
    path('user/login', views.UserLogin.as_view()),
    path('user/logout', views.UserLogout.as_view()),
    path('user/register', views.UserRegister.as_view()),
    path('user/forget', views.UserForgetPwd.as_view()),
    path('user/<int:uid>', views.UserView.as_view()),
]